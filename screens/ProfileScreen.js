import React from 'react';
import { StyleSheet, Text, View, StatusBar, ImageBackground, Image, ScrollView } from 'react-native';
import { Header, Left, Right, Icon, Toast, Input, Card, Root, CardItem } from 'native-base';
import { Button } from 'react-native-elements';
import firebase from 'firebase';
import CardSection from './components/cardsection';
import Expo from "expo";
import { Pedometer } from "expo";
import { Hoshi } from 'react-native-textinput-effects';

const user = {};
const METAS = [];

let userAvatar;
export default class ProfileScreen extends React.Component {
  state = {
    nombre: '', edad: 0, peso: 0, currentUser: null, email: null, estatura: null, metas: [],
    isPedometerAvailable: "checking",
    pastStepCount: 0,
    currentStepCount: 0,
    editmode: false,

  }
  componentDidMount() {
    this._subscribe();
    const { currentUser } = this.state;
  }
  componentWillUnmount() {
    this._unsubscribe();
  }
  componentWillMount() {
    const { currentUser } = firebase.auth()
    this.setState({ currentUser })
    firebase.database().ref('/usuarios/' + currentUser.uid).once('value', (snapshot) => {
      user = snapshot.val();
      metas = user.metas;
      userAvatar = user.sexo === "Masculino" ? require('../assets/avatarMan.jpg') : require('../assets/avatar.png');
      METAS = [];
      metasArray = [];
      if (metas.constructor === Array) {
        metasArray = metas;
      } else {
        Object.keys(metas).map((meta) => {
          metasArray.push(metas[meta]);
        })
      }
      metasArray.forEach(function (value, key) {
        METAS.push(key + " - " + value);
      });
      this.setState({ nombre: user.nombre, edad: user.edad, email: user.email, metas: METAS, estatura: user.estatura });
    })

    var year = (new Date()).getFullYear()
    var ultimoPeso;
    firebase.database().ref('/progreso/usuarios/' + currentUser.uid + '/' + year + '/')
      .once('value', (snapshot) => {
        var pesosArray = []
        var peso = snapshot.val()
        try {
          Object.keys(peso).map(function (key) {
            pesosArray.push(
              peso[key]
            );
          });
        } catch (err) { }
        pesosArray.forEach(element => {
          var pesosSubArray = []
          var pesoElement = element.peso
          try {
            Object.keys(pesoElement).map(function (key) {
              pesosSubArray.push(
                pesoElement[key]
              );
            });
          } catch (err) { }

          pesosSubArray.forEach(element => {
            ultimoPeso=element.peso
            console.log(element)
          });
        });
        this.setState({peso:ultimoPeso})
      })
  }

  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <Icon name="person" style={{ fontSize: 24, color: tintColor }} />
    )
  }
  _subscribe = () => {
    this._subscription = Pedometer.watchStepCount(result => {
      this.setState({
        currentStepCount: result.steps
      });
    });

    Pedometer.isAvailableAsync().then(
      result => {
        this.setState({
          isPedometerAvailable: String(result)
        });
      },
      error => {
        this.setState({
          isPedometerAvailable: "Could not get isPedometerAvailable: " + error
        });
      }
    );

    const end = new Date();
    const start = new Date();
    start.setDate(end.getDate() - 1);
    Pedometer.getStepCountAsync(start, end).then(
      result => {
        this.setState({ pastStepCount: result.steps });
      },
      error => {
        this.setState({
          pastStepCount: "Could not get stepCount: " + error
        });
      }
    );
  };
  _unsubscribe = () => {
    this._subscription && this._subscription.remove();
    this._subscription = null;
  };

  editUser() {
    const { currentUser } = firebase.auth()
    Toast.show({
      text: "Editado correctamente",
      textStyle: { color: "green", fontFamily: "Roboto" },
      buttonText: "Ok",
      duration: 3000
    })
    firebase.database().ref('/usuarios/' + currentUser.uid).update({
      nombre: this.state.nombre,
      estatura: this.state.estatura,
      edad: this.state.edad,
      peso: this.state.peso
    }).then(() => {
      this.setState({ editmode: false })
    })


  }

  userInfo() {
    if (this.state.editmode === true) {
      return (
        // <View style={{
        //   height: 450,
        //   flex: 1,
        //   flexDirection: 'column',
        //   justifyContent: 'space-around'
        // }}
        // >
        <ScrollView style={{ height: '100%', paddingBottom: 30, marginBottom: 20 }}>
          <Hoshi value={this.state.nombre} borderColor={'#000000'} label={'Nombre'} onChangeText={nombre => this.setState({ nombre })} />
          <Hoshi value={this.state.estatura} borderColor={'#000000'} keyboardType='numeric' label={'Estatura'} onChangeText={estatura => this.setState({ estatura })} />
          <Hoshi value={this.state.edad} borderColor={'#000000'} keyboardType='numeric' label={'Edad'} onChangeText={edad => this.setState({ edad })} />
          <Button
            rounded
            title="GUARDAR"
            onPress={this.editUser.bind(this)}
            buttonStyle={{
              marginTop: 20,
              backgroundColor: "#00AA00",
              width: '100%'
            }} />
          <Button
            rounded
            title="CANCELAR"
            onPress={() => { this.setState({ editmode: false }) }}
            buttonStyle={{
              marginTop: 20,
              backgroundColor: "#545aa1",
              width: '100%'
            }} />
        </ScrollView>

        // </View>

      )
    } else {
      return (
        // <View style={{
        //   height: 250,
        //   flex: 1,
        //   flexDirection: 'column',
        //   justifyContent: 'space-around'
        // }}
        // >
        <ScrollView style={{ height: '100%', paddingBottom: 30, marginBottom: 20 }}>
          <Card>
            <CardItem bordered>
              <Text style={{ fontWeight: 'bold' }}>Nombre </Text><Text style={{ fontSize: 16 }}> {this.state.nombre}</Text>
            </CardItem>
            <CardItem bordered>
              <Text style={{ fontWeight: 'bold' }}>Email </Text><Text style={{ fontSize: 16 }}>{this.state.email}</Text>
            </CardItem >
            <CardItem bordered>
              <Text style={{ fontWeight: 'bold' }}>Edad </Text><Text style={{ fontSize: 16 }}> {this.state.edad} años</Text>
            </CardItem>
            <CardItem bordered>
              <Text style={{ fontWeight: 'bold' }}>Estatura </Text><Text style={{ fontSize: 16 }}>{this.state.estatura} metros</Text>
            </CardItem>
            <CardItem bordered>
              <Text style={{ fontWeight: 'bold' }}>Peso </Text><Text style={{ fontSize: 16 }}>{this.state.peso} kg</Text>
            </CardItem>
            <CardItem bordered>
              <Text style={{ fontWeight: 'bold' }}>Pasos </Text><Text style={{ fontSize: 16 }}> {this.state.currentStepCount} pasos</Text>
            </CardItem>
          </Card>

          {/* <Text style={{ fontWeight: 'bold' }}>Nombre:</Text><Text style={{ fontSize: 20 }}> {this.state.nombre}</Text><Text style={{ fontWeight: 'bold' }}>Email:</Text><Text style={{ fontSize: 20 }}>{this.state.email}</Text>
          <Text style={{ fontWeight: 'bold' }}>Estatura:</Text> <Text style={{ fontSize: 20 }}>{this.state.estatura} metros</Text>
          <Text style={{ fontWeight: 'bold' }}>Edad: </Text><Text style={{ fontSize: 20 }}>{this.state.edad} años</Text>
          <Text style={{ fontWeight: 'bold' }}>Peso:</Text> <Text style={{ fontSize: 20 }}>{this.state.peso} kg</Text>
          <Text style={{ fontWeight: 'bold' }}>Pasos: </Text><Text style={{ fontSize: 20 }}> {this.state.currentStepCount} pasos</Text> */}

          <Button
            rounded
            title="EDITAR PROFILE"
            onPress={() => { this.setState({ editmode: true }) }}
            buttonStyle={{
              marginTop: 20,
              backgroundColor: "#545aa1",
              width: '100%',
            }} />
          {/* </View> */}
        </ScrollView>
      )
    }

  }

  render() {
    return (
      <Root>
        <View>
          <Header>
            <Left style={{ flex: 1 }}>
              <Icon name="menu" onPress={() => this.props.navigation.openDrawer()} />
            </Left>
          </Header>
          <ImageBackground style={{ width: '100%', height: 150 }} source={require('../assets/background.jpg')}>
            <View style={{ height: 150, alignItems: 'center', justifyContent: 'center' }}>
              <Image source={userAvatar} style={{ height: 120, width: 120, borderRadius: 60 }}></Image>
            </View>
          </ImageBackground>

          <Card>
            <CardSection>
              {this.userInfo()}
            </CardSection>
          </Card>
        </View>
      </Root>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
