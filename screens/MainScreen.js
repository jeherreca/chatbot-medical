import React from 'react';
import { AppRegistry, Text, View, StatusBar, KeyboardAvoidingView, Image } from 'react-native';
import { Left, Right, Icon, ActivityIndicator } from 'native-base';
import firebase from 'firebase';
import { Button } from 'react-native-elements';
import Card from './components/card.js';
import CardSection from './components/cardsection.js';
import Spinner from './components/Spinner.js';
import Header from './components/header.js';
import { createStackNavigator, StackNavigator } from 'react-navigation';
import LoginScreen from './LoginScreen';
import ChatBot from 'react-native-chatbot';


let steps;
export default class MainScreen extends React.Component {
    state = { main: true };




    renderOrNot() {
        if (this.state.main === true) {
            return (
                <View>
                    <ChatBot steps={
                        [
                            {
                                id: 1,
                                message: 'Hola!',
                                trigger: 2
                            },
                            {
                                id: 2,
                                message: 'Botic es una plataforma de seguimiento y compañamiento de pacientes con diabetes tipo 2.',
                                trigger: 3
                            },
                            {
                                id: 3,
                                message: 'Creada para ayudarlos a completar los objetivos alimenticios propuestos por su personal medico especialista',
                                trigger:4
                            },
                            {
                                id:4,
                                message:'Pulsa el boton e inicia sesion!',
                                trigger:5,
                            },
                            {
                                id:5,
                                component:(<Button
                                    rounded
                                    onPress={() => this.setState({ main: false })}
                                    title="INICIAR SESION"
                                    buttonStyle={{
                                        backgroundColor: "#545aa1",
                                        width: 300,
                                    }}
                                />),
                                end:true
                            }
                        ]
                    } />
                </View>

            )
        } else {
            return (
                <KeyboardAvoidingView
                behavior='padding'
                keyboardVerticalOffset={-64}
              >
                <LoginScreen />
                <Button
                    rounded
                    onPress={() => this.setState({ main: true })}
                    title="ATRAS"
                    buttonStyle={{
                        alignSelf: 'center',
                        backgroundColor: "#545aa1",
                        width: 300
                    }}
                />
                
            </KeyboardAvoidingView>)
        }
    }
    render() {
        return (
			<KeyboardAvoidingView behavior="padding" enabled>
            
                {/* <Header headerText={'Bienvenido'} /> */}
                {this.renderOrNot()}
            </KeyboardAvoidingView>
        );
    }


}