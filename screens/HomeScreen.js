import React from 'react';
import { StyleSheet, Text, View, StatusBar, Platform, Alert } from 'react-native';
import ChatBot from 'react-native-chatbot';
import { Button } from 'react-native-elements';
import { StackNavigator } from 'react-navigation';
import RespuestaChatBot from './components/respuestaChatBot';
import { Header, Left, Right, Icon, Spinner, Switch } from 'native-base';
import firebase from 'firebase';
import moment from 'moment';
import { consolidateStreamedStyles } from 'styled-components';
import ReUse from './components/ReUse';
const user = '';
const METAS = [];
const steps = [];
let ejerciciosHoy = "";
var didmount = false;
export default class HomeScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = { reUse: [false, false, false, false, false, false, false, false, false, false, false, false], score: [], currentUser: null, email: null, estatura: null, metas: [], respuestasMetas: [], loaded: 0, nombre: null }
    this.changeReuse = this.changeReuse.bind(this)
  }

  componentWillMount() {
    const { currentUser } = firebase.auth()
    this.setState({ currentUser })
    var year = moment().format('YYYY');
    var week = moment().format('WW');
    var day = moment().format('E');

    firebase.database().ref('/usuarios/' + currentUser.uid).once('value', (snapshot) => {
      user = snapshot.val();
      metas = user.metas;
      METAS = [];
      metasArray = [];
      if (metas.constructor === Array) {
        for (let index = 0; index < metas.length; index++) {
          const element = metas[index];
          if (typeof metas[index] !== 'undefined') {
            metasArray.push({ 'meta': element, key: index })
            METAS.push(index)
          }
        }
      } else {
        Object.keys(metas).map((meta) => {
          metasArray.push(metas[meta]);
          METAS.push(Number(meta));
        })
      }

      this.setState({ metas: METAS, nombre: user.nombre });
      var respuestasMetas = [];
      var that = this;
      var contWillMount = 0;

      metasArray = [];
      if (metas.constructor === Array) {
        for (let index = 0; index < metas.length; index++) {
          const element = metas[index];
          if (typeof metas[index] !== 'undefined') {
            metasArray.push({ 'meta': element, key: index })
          }
        }
      } else {
        Object.keys(metas).map((meta) => {
          metasArray.push({ 'meta': metas[meta], key: meta });
        })
      }
      var pesoArray = [];
      firebase.database().ref('/progreso/usuarios/' + currentUser.uid + '/' + year + '/' + week + '/peso/')
        .once('value', (snapshot) => {
          var pesosArray = [];
          var peso;
          if (snapshot !== null) {
            peso = snapshot.val();
            try {
              console.log("sp",snapshot.val())
              Object.keys(peso).map(function (key) {
                pesosArray.push(
                  peso[key]
                );
              });
            } catch (err) { }
            console.log(peso);
            var keyObj = "-1";
            try {
              keyObj = Object.keys(snapshot.val())[Object.keys(snapshot.val()).length - 1];
            } catch (err) { }
            var respPeso={
              pesos:peso,
              ultimaRespuesta:keyObj
            }
            this.setState({ respuestasPeso: respPeso })
          }
        })

      metasArray.forEach(function (value) {
        var key = value.key;
        firebase.database().ref('/progreso/usuarios/' + currentUser.uid + '/' + year + '/' + week + '/metas/' + key + '/')
          .once('value', (snapshot) => {
            var progresoArray = [];
            if (snapshot !== null) {
              progreso = snapshot.val();
              
              try {
                progreso = Object.keys(progreso).map(function (key) {
                  progresoArray.push(
                    progreso[key]
                  );
                });
              } catch (err) { }
            }

            progreso = progresoArray;
            var respuestas = 0;
            if (progreso !== null) {
              progreso.forEach(function (element, key2) {
                respuestas += Number(element["respuesta"]);

              });
              var keyObj = "-1";
              try {
                keyObj = Object.keys(snapshot.val())[Object.keys(snapshot.val()).length - 1];
              } catch (err) { }

              // var progresoObject = progreso[keyObj + ""];
              // var progresoScore = progresoObject["score"];
              respuestasMetas[key] = {
                descripcion: value,
                ultimaRespuesta: keyObj,
                respuestas: respuestas
              };

              //  this.setState({ metas: METAS });
              contWillMount++;
            }
            that.setState({ respuestasMetas: respuestasMetas, loaded: contWillMount })
          }).then(

          );
      });
      metasArray = [];
      if (metas.constructor === Array) {
        for (let index = 0; index < metas.length; index++) {
          const element = metas[index];
          if (typeof metas[index] !== 'undefined') {
            metasArray.push({ 'meta': element, key: index })
          }
        }
      } else {
        Object.keys(metas).map((meta) => {
          metasArray.push({ 'meta': metas[meta], key: meta });
        })
      }
      metasArray.forEach(function (value) {
        var key = value.key
        firebase.database().ref('/progreso/usuarios/' + currentUser.uid + '/' + year + '/' + (week - 1) + '/metas/' + key + '/')
          .once('value', (snapshot) => {
            progreso = snapshot.val();
            var score = [];
            if (progreso !== null) {
              var progresoArray = [];
              progreso = Object.keys(progreso).map(function (key) {
                progresoArray.push(
                  progreso[key]
                );
              });
              progreso = progresoArray;
              var respuestas = 0;
              progreso.forEach(function (element) {
                respuestas += Number(element["respuesta"]);
              });
              switch (key) {
                case 1:
                  score[key] = respuestas / 150 * 100;
                  break;
                case 2:
                  score[key] = respuestas / 2 * 100;
                  break;
                case 3:
                  score[key] = respuestas / 35 * 100;
                  break;
                case 4:
                  score[key] = respuestas / 21 * 100;
                  break;
                case 5:
                  score[key] = respuestas / 28 * 100;
                  break;
                default:
                  score[key] = 100;
                  break;
              }
            } else {
              score[key] = 100;
            }
            // contWillMount++;
            that.setState({ score: score })
          }).then(

          );
      });
    })



  }

  componentDidUpdate() {
    this.didmount = true;
  }


  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <Icon name="home" style={{ fontSize: 24, color: tintColor }} />
    )
  }



  render() {
    if (this.state.loaded >= 3) {
      return (
        <View style={{ height: '100%' }}>
          <Header>
            <Left style={{ flex: 1 }}>
              <Icon name="menu" onPress={() => this.props.navigation.openDrawer()} />
            </Left>
          </Header>
          {
            this.displayChatBot(this.state.reUse)
          }
        </View>
      )
    } else {
      return null;
    }

  }

  ejerciciosHoy = false;

  changeReuse(id) {
    var temp = [false, false, false, false, false, false, false, false, false, false, false, false]
    temp[id] = true
    this.setState({
      reUse: temp
    })
  }
  validateDateOfQuestion(meta, reUse) {
    try {
      var returnBool = false;
      if (meta !== -1) var objMeta = this.state.respuestasMetas[meta];
      else var objMeta = this.state.respuestasPeso;
      console.log("rpPeso",objMeta)
      if (objMeta.ultimaRespuesta !== "-1") {
        var date = new Date(Number(objMeta.ultimaRespuesta));
        var todayDay = new Date();
        var tday = moment(todayDay).format("DD-MM-YYYY");
        var day = moment(date).format("DD-MM-YYYY");
        var week = moment(date).format("WW");
        var tweek = moment(todayDay).format("WW");
        console.log("week and tweek",week,tweek)
        var hora = moment(date).format("HH");
        // var day=moment(objMeta.ultimaRespuesta).format('dd HH');
        switch (meta) {
          case -1:
            if (week !== tweek) returnBool = true;
            break;
          case 1:
            if ((day !== tday && Number(objMeta.respuestas) < 150) || reUse[1]) returnBool = true;
            break;

          case 2:
            if ((day !== tday && Number(objMeta.respuestas) < 2) || reUse[2]) returnBool = true;
            break;

          case 3:
            if ((day !== tday && Number(objMeta.respuestas) < 35) || reUse[3]) returnBool = true;
            break;

          case 4:
            if ((day !== tday && Number(objMeta.respuestas) < 21) || reUse[4]) returnBool = true;
            break;

          case 5:
            if ((day !== tday && Number(objMeta.respuestas) < 28) || reUse[5]) returnBool = true;
            break;

          case 6:
            if ((day !== tday && Number(objMeta.respuestas) < 3) || reUse[6]) returnBool = true;
            break;

          case 7:
            if ((day !== tday && Number(objMeta.respuestas) < 28) || reUse[7]) returnBool = true;
            break;

          case 8:
            if ((day !== tday && Number(objMeta.respuestas) < 7) || reUse[8]) returnBool = true;
            break;

          case 9:
            if (day !== tday || reUse[9]) returnBool = true;
            break;

          case 10:
            if (day !== tday || reUse[10]) returnBool = true;
            break;

          case 11:
            if (day !== tday || reUse[11]) returnBool = true;
            break;

          case 12:
            if (day !== tday || reUse[12]) returnBool = true;
            break;
        }
        return returnBool;
      } else {
        return true;
      }

    } catch (err) {
      console.log("err");
    }

  }


  displayChatBot(reuse) {

    const { metas } = this.state;
    let idCont = 12;
    let steps = [];
    const { score } = this.state;

    if (metas.includes(1) && this.validateDateOfQuestion(1, reuse)) {
      steps = [
        {
          id: '1',
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Hola, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return '¿Como estas, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return 'Saludos, ' + this.state.nombre
                } else {
                  return 'Saludos, ' + this.state.nombre
                }
              }
            }
          },
          trigger: '2',
        },
        {
          id: '2',
          message: 'Cuantos minutos de ejercicios has hecho hoy?',
          trigger: 3
        },
        {
          id: '3',
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={22} currentStep={3} meta={1} menorQueCorrecto={false} />),
          waitAction: true
        },

        {
          id: '4',
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[1] <= 50) {
              if (numero == 1) {
                return 'Eso está mejor'
              } else {
                if (numero == 2) {
                  return 'Sigue mejorando'
                } else {
                  return 'Sigue mejorando'
                }
              }
            } else {
              if (numero == 1) {
                return 'Que bien'
              } else {
                if (numero == 2) {
                  return 'Genial'
                } else {
                  return 'Genial'
                }
              }
            }
          },
          trigger: '5',
        },
        {
          id: '5',
          message: 'Recuerda que el ejercicio es fundamental para estar saludable',
          trigger: '6',
        },
        {
          id: '6',
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'El ejercicio ayuda a controlar que se aumente la sensibilidad a la insulina, por lo que las células pueden aprovechar más cualquier insulina disponible.'
            } else {
              if (numero == 2) {
                return 'El ejercicio ayuda a que te sientas con más energía ya fortalecer tu cuerpo.'
              } else {
                return 'El ejercicio ayuda a que te sientas con más energía ya fortalecer tu cuerpo.'
              }
            }
          },
          trigger: '7',
        },
        {
          id: '7',
          message: 'Continua así todos los dias y veras los resultados.',
          trigger: idCont
        },
        {
          id: '8',
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[1] <= 50) {
              return 'No está bien que no los estés haciendo'
            } else {
              if (numero == 1) {
                return 'Anímate a hacerlos'
              } else {
                if (numero == 2) {
                  return '¿Por que tan pocos ?'
                } else {
                  return '¿Por que tan pocos ?'
                }
              }
            }
          },
          trigger: '9',
        },
        {
          id: '9',
          message: 'Sin hacer ejercicio es muy difícil lograr resultados',
          trigger: '10',
        },

        {
          id: '10',
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'El ejercicio ayuda a controlar que se aumente la sensibilidad a la insulina, por lo que las células pueden aprovechar más cualquier insulina disponible.'
            } else {
              if (numero == 2) {
                return 'El ejercicio ayuda a que te sientas con más energía ya fortalecer tu cuerpo.'
              } else {
                return 'El ejercicio ayuda a que te sientas con más energía ya fortalecer tu cuerpo.'
              }
            }
          },
          trigger: '11',
        },
        {
          id: '11',
          message: () => {
            if (score[1] <= 50) {
              return 'No esta bien que no los estes haciendo'
            } else {
              return 'Hazlos cuando puedas'
            }
          },
          trigger: idCont
        },
      ];

    }

    if (metas.includes(2) && this.validateDateOfQuestion(2, reuse)) {
      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Dime, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return 'Cuéntame, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return '¡' + this.state.nombre + ' !'
                } else {
                  return '¡' + this.state.nombre + ' !'
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿ Hoy cuantos ejercicios de intensidad moderada realizaste ?',
          trigger: idCont + 2,
        },
        {
          id: idCont + 2,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={1} currentStep={(idCont + 2)} meta={2} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: idCont + 3,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[2] <= 50) {
              if (numero == 1) {
                return 'Eso está mejor'
              } else {
                if (numero == 2) {
                  return 'Sigue así'
                } else {
                  return 'Sigue así'
                }
              }
            } else {
              if (numero == 1) {
                return 'Excelente'
              } else {
                if (numero == 2) {
                  return 'Me alegra'
                } else {
                  return 'Me alegra'
                }
              }
            }
          },
          trigger: idCont + 4,
        },
        {
          id: idCont + 4,
          message: 'Aumentar la intensidad de los ejercicios es bueno para la salud de tu cuerpo',
          trigger: idCont + 5,
        },
        {
          id: idCont + 5,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'El ejercicio permite estimular otro mecanismo que ayuda al control del azúcar, diferente al de la insulina, especialmente cuando se contraen los músculos.'
            } else {
              if (numero == 2) {
                return 'El ejercicio ayuda a que te sientas con más energía ya fortalecer tu cuerpo.'
              } else {
                return 'El ejercicio ayuda a que te sientas con más energía ya fortalecer tu cuerpo.'
              }
            }
          },
          trigger: idCont + 6
        },
        {
          id: idCont + 6,
          message: 'Prueba ir variando los ejercicios y sigue asi.',
          trigger: idCont + 11,
        },
        {
          id: idCont + 7,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[2] <= 50) {
              return 'Recuerda que necesitas mejorar'
            } else {
              if (numero == 1) {
                return 'Intenta otro dia'
              } else {
                if (numero == 2) {
                  return 'Bueno'
                } else {
                  return 'Bueno'
                }
              }
            }
          },
          trigger: idCont + 8,
        },
        {
          id: idCont + 8,
          message: 'Recuerda que con solo dos dias a la semana es suficiente',
          trigger: idCont + 9,
        },
        {
          id: idCont + 9,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'El ejercicio permite estimular otro mecanismo que ayuda al control del azúcar, diferente al de la insulina, especialmente cuando se contraen los músculos.'
            } else {
              if (numero == 2) {
                return 'El ejercicio ayuda a que te sientas con más energía ya fortalecer tu cuerpo.'
              } else {
                return 'El ejercicio ayuda a que te sientas con más energía ya fortalecer tu cuerpo.'
              }
            }
          },
          trigger: idCont + 10,
        },
        {
          id: idCont + 10,
          message: () => {
            if (score[2] <= 50) {
              return 'Si no te sientes bien, llama a tu médico'
            } else {
              return 'Dale con ganas'
            }
          },
          trigger: idCont + 11,
        },
      );

      idCont = idCont + 11;

    }






    if (metas.includes(4) && this.validateDateOfQuestion(4, reuse)) {

      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Hola, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return '¿Como estas, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return 'Saludos, ' + this.state.nombre
                } else {
                  return 'Saludos, ' + this.state.nombre
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿ Cuantas porciones de frutas has comido ?',
          trigger: idCont + 2,
        },
        {
          id: idCont + 2,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={3} currentStep={(idCont + 2)} meta={4} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: idCont + 3,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[4] <= 50) {
              if (numero == 1) {
                return '¡Eso es !'
              } else {
                if (numero == 2) {
                  return 'Bien hecho, has mejorado'
                } else {
                  return 'Bien hecho, has mejorado'
                }
              }
            } else {
              if (numero == 1) {
                return 'Genial'
              } else {
                if (numero == 2) {
                  return 'Muy bien'
                } else {
                  return 'Muy bien'
                }
              }
            }
          },
          trigger: idCont + 4,
        },
        {
          id: idCont + 4,
          message: 'Las frutas son muy ricas y saludables',
          trigger: idCont + 5,
        },
        {
          id: idCont + 5,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Cada una tiene un beneficio diferente que nos ayuda, como eliminar el colesterol o eliminar toxinas.'
            } else {
              if (numero == 2) {
                return 'Son una fuente de energía natural y ayudan al proceso digestivo.'
              } else {
                return 'Son una fuente de energía natural y ayudan al proceso digestivo.'
              }
            }
          },
          trigger: idCont + 6
        },
        {
          id: idCont + 6,
          message: 'No dejes de probar frutas diferentes',
          trigger: idCont + 11,
        },
        {
          id: idCont + 7,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[4] <= 50) {
              return 'Estas comiendo mal'
            } else {
              if (numero == 1) {
                return 'Muy poco'
              } else {
                if (numero == 2) {
                  return 'Tienes que comer más'
                } else {
                  return 'Tienes que comer más'
                }
              }
            }
          },
          trigger: idCont + 8,
        },
        {
          id: idCont + 8,
          message: 'Las frutas son perfectas como merienda y desayuno',
          trigger: idCont + 9,
        },
        {
          id: idCont + 9,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Si lo haces en ese momento te sentirás mejor a lo largo del día.'
            } else {
              if (numero == 2) {
                return 'Aportan facilidades para la digestión, lo que ayuda a eliminar toxinas y limpiar nuestro cuerpo.'
              } else {
                return 'Aportan facilidades para la digestión, lo que ayuda a eliminar toxinas y limpiar nuestro cuerpo.'
              }
            }
          },
          trigger: idCont + 10,
        },
        {
          id: idCont + 10,
          message: () => {
            if (score[4] <= 50) {
              return 'Encuentra frutas que te gusten'
            } else {
              return 'Así que son muy importante para tu cuerpo'
            }
          },
          trigger: idCont + 11,
        },
      );

      idCont = idCont + 11;

    }


    if (metas.includes(3) && this.validateDateOfQuestion(3, reuse)) {
      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Hola, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return '¿Como estas, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return 'Saludos, ' + this.state.nombre
                } else {
                  return 'Saludos, ' + this.state.nombre
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿ Cuantas porciones de verduras has comido ?',
          trigger: idCont + 2,
        },
        {
          id: idCont + 2,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={5} currentStep={(idCont + 2)} meta={3} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: idCont + 3,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[3] <= 50) {
              if (numero == 1) {
                return '¡Eso es !'
              } else {
                if (numero == 2) {
                  return 'Bien hecho, has mejorado'
                } else {
                  return 'Bien hecho, has mejorado'
                }
              }
            } else {
              if (numero == 1) {
                return 'Genial'
              } else {
                if (numero == 2) {
                  return 'Muy bien'
                } else {
                  return 'Muy bien'
                }
              }
            }
          },
          trigger: idCont + 4,
        },
        {
          id: idCont + 4,
          message: 'Las verduras aportan gran cantidad de nutrientes a nuestra dieta',
          trigger: idCont + 5,
        },
        {
          id: idCont + 5,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Además, ayudan a limpiar el organismo gracias a que son excelentes antioxidantes y son de fácil digestión.'
            } else {
              if (numero == 2) {
                return 'Además, aportan color y textura a nuestras comidas.'
              } else {
                return 'Además, aportan color y textura a nuestras comidas.'
              }
            }
          },
          trigger: idCont + 6
        },
        {
          id: idCont + 6,
          message: 'No dejes de comerlas todos los días.',
          trigger: idCont + 11,
        },
        {
          id: idCont + 7,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[3] <= 50) {
              return 'Estas comiendo mal'
            } else {
              if (numero == 1) {
                return 'Muy poco'
              } else {
                if (numero == 2) {
                  return 'Tienes que comer más'
                } else {
                  return 'Tienes que comer más'
                }
              }
            }
          },
          trigger: idCont + 8,
        },
        {
          id: idCont + 8,
          message: 'Así no te guste el sabor hazlo por tu salud',
          trigger: idCont + 9,
        },
        {
          id: idCont + 9,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Las verduras ayudan a limpiar el organismo gracias a que son excelentes antioxidantes y son de fácil digestión.'
            } else {
              if (numero == 2) {
                return 'Las verduras aportan color y textura a nuestras comidas'
              } else {
                return 'Las verduras aportan color y textura a nuestras comidas'
              }
            }
          },
          trigger: idCont + 10,
        },
        {
          id: idCont + 10,
          message: () => {
            if (score[3] <= 50) {
              return 'Encuentra frutas que te gusten'
            } else {
              return 'Así que son muy importante para tu cuerpo'
            }
          },
          trigger: idCont + 11,
        },
      );

      idCont = idCont + 11;

    }

    if (metas.includes(5) && this.validateDateOfQuestion(5, reuse)) {
      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Dime, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return 'Cuéntame, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return '¡' + this.state.nombre + ' !'
                } else {
                  return '¡' + this.state.nombre + ' !'
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿ Cuantas cucharadas de aceite de oliva extra virgen has tomado ?',
          trigger: idCont + 2,
        },
        {
          id: idCont + 2,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={4} currentStep={(idCont + 2)} meta={5} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: idCont + 3,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[5] <= 50) {
              if (numero == 1) {
                return 'Eso esta mejor'
              } else {
                if (numero == 2) {
                  return 'Sigue así'
                } else {
                  return 'Sigue así'
                }
              }
            } else {
              if (numero == 1) {
                return 'Genial'
              } else {
                if (numero == 2) {
                  return 'Que bien'
                } else {
                  return 'Que bien'
                }
              }
            }
          },
          trigger: idCont + 4,
        },
        {
          id: idCont + 4,
          message: 'El aceite de olive ofrece varios beneficios',
          trigger: idCont + 5,
        },
        {
          id: idCont + 5,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Favorece el buen funcionamiento del aparato digestivo y ayuda a adelgazar'
            } else {
              if (numero == 2) {
                return 'Estimula el buen funcionamiento de la vesícula.'
              } else {
                return 'Estimula el buen funcionamiento de la vesícula.'
              }
            }
          },
          trigger: idCont + 6
        },
        {
          id: idCont + 6,
          message: 'Sigue haciéndolo',
          trigger: idCont + 11,
        },
        {
          id: idCont + 7,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[5] <= 50) {
              return 'No te cuesta nada'
            } else {
              if (numero == 1) {
                return 'Intentalo'
              } else {
                if (numero == 2) {
                  return 'Es facil y rápido'
                } else {
                  return 'Es facil y rápido'
                }
              }
            }
          },
          trigger: idCont + 8,
        },
        {
          id: idCont + 8,
          message: 'Te ofrece grandes beneficios, muy útiles para tu cuerpo',
          trigger: idCont + 9,
        },
        {
          id: idCont + 9,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Favorece el buen funcionamiento del aparato digestivo y ayuda a adelgazar'
            } else {
              if (numero == 2) {
                return 'Estimula el buen funcionamiento de la vesícula.'
              } else {
                return 'Estimula el buen funcionamiento de la vesícula.'
              }
            }
          },
          trigger: idCont + 10,
        },
        {
          id: idCont + 10,
          message: () => {
            if (score[5] <= 50) {
              return 'Animáte'
            } else {
              return 'No pierdes nada en intentarlo'
            }
          },
          trigger: idCont + 11,
        },
      );

      idCont = idCont + 11;

    }

    if (metas.includes(6) && this.validateDateOfQuestion(6, reuse)) {
      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Dime, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return 'Cuéntame, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return '¡' + this.state.nombre + ' !'
                } else {
                  return '¡' + this.state.nombre + ' !'
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿ Cuantas porciones de frutos secos has comido ?',
          trigger: idCont + 2,
        },
        {
          id: idCont + 2,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={3} currentStep={(idCont + 2)} meta={6} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: idCont + 3,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[6] <= 50) {
              if (numero == 1) {
                return 'Eso esta mejor'
              } else {
                if (numero == 2) {
                  return 'Sigue así'
                } else {
                  return 'Sigue así'
                }
              }
            } else {
              if (numero == 1) {
                return 'Genial'
              } else {
                if (numero == 2) {
                  return 'Que bien'
                } else {
                  return 'Que bien'
                }
              }
            }
          },
          trigger: idCont + 4,
        },
        {
          id: idCont + 4,
          message: 'Los frutos secos son una excelente merienda, y una gran opción cuando tengas hambre y no sepas qué comer.',
          trigger: idCont + 5,
        },
        {
          id: idCont + 5,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Aportan gran cantidad de minerales'
            } else {
              if (numero == 2) {
                return 'Son ricos en fibra'
              } else {
                return 'Son ricos en fibra'
              }
            }
          },
          trigger: idCont + 6
        },
        {
          id: idCont + 6,
          message: 'No dejes de comerlos ',
          trigger: idCont + 11,
        },
        {
          id: idCont + 7,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[6] <= 50) {
              return 'Espero que al menos no estes comiendo porquerías'
            } else {
              if (numero == 1) {
                return 'Intentalo'
              } else {
                if (numero == 2) {
                  return 'Son muy ricos'
                } else {
                  return 'Son muy ricos'
                }
              }
            }
          },
          trigger: idCont + 8,
        },
        {
          id: idCont + 8,
          message: 'Es preferible que comas eso a otro tipo de snacks mas dañinos',
          trigger: idCont + 9,
        },
        {
          id: idCont + 9,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Aportan gran cantidad de minerales'
            } else {
              if (numero == 2) {
                return 'Son ricos en fibra'
              } else {
                return 'Son ricos en fibra'
              }
            }
          },
          trigger: idCont + 10,
        },
        {
          id: idCont + 10,
          message: () => {
            if (score[6] <= 50) {
              return 'Tienes que intentar cumplir tus objetivos'
            } else {
              return 'No falles la proxíma'
            }
          },
          trigger: idCont + 11,
        },
      );

      idCont = idCont + 11;

    }

    if (metas.includes(7) && this.validateDateOfQuestion(7, reuse)) {
      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Dime, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return 'Cuéntame, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return '¡' + this.state.nombre + ' !'
                } else {
                  return '¡' + this.state.nombre + ' !'
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿ Cuantos alimentos integrales has comido hoy ?',
          trigger: idCont + 2,
        },
        {
          id: idCont + 2,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={4} currentStep={(idCont + 2)} meta={7} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: idCont + 3,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[7] <= 50) {
              if (numero == 1) {
                return 'Eso esta mejor'
              } else {
                if (numero == 2) {
                  return 'Sigue así'
                } else {
                  return 'Sigue así'
                }
              }
            } else {
              if (numero == 1) {
                return 'Genial'
              } else {
                if (numero == 2) {
                  return 'Que bien'
                } else {
                  return 'Que bien'
                }
              }
            }
          },
          trigger: idCont + 4,
        },
        {
          id: idCont + 4,
          message: 'Los productos integrales son lo mejor para reemplazar productos altos en grasas y harinas',
          trigger: idCont + 5,
        },
        {
          id: idCont + 5,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Es preferible a productos grasos'
            } else {
              if (numero == 2) {
                return 'Son ricos en fibra'
              } else {
                return 'Son ricos en fibra'
              }
            }
          },
          trigger: idCont + 6
        },
        {
          id: idCont + 6,
          message: 'Sigue comiendo este tipo de alimentos ',
          trigger: idCont + 11,
        },
        {
          id: idCont + 7,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[5] <= 50) {
              return 'Espero que al menos no estes comiendo porquerías'
            } else {
              if (numero == 1) {
                return 'Intentalo'
              } else {
                if (numero == 2) {
                  return 'Son muy ricos'
                } else {
                  return 'Son muy ricos'
                }
              }
            }
          },
          trigger: idCont + 8,
        },
        {
          id: idCont + 8,
          message: 'Es preferible que comas eso a otro tipo de productos mas dañinos',
          trigger: idCont + 9,
        },
        {
          id: idCont + 9,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Es preferible a productos grasos'
            } else {
              if (numero == 2) {
                return 'Son ricos en fibra'
              } else {
                return 'Son ricos en fibra'
              }
            }
          },
          trigger: idCont + 10,
        },
        {
          id: idCont + 10,
          message: () => {
            if (score[7] <= 50) {
              return 'Tienes que intentar cumplir tus objetivos'
            } else {
              return 'No falles la proxíma'
            }
          },
          trigger: idCont + 11,
        },
      );

      idCont = idCont + 11;

    }

    if (metas.includes(8) && this.validateDateOfQuestion(8, reuse)) {
      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Dime, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return 'Cuéntame, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return '¡' + this.state.nombre + ' !'
                } else {
                  return '¡' + this.state.nombre + ' !'
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿ Cuantos productos lácteos bajos en grasa has comido hoy ?',
          trigger: idCont + 2,
        },
        {
          id: idCont + 2,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={1} currentStep={(idCont + 2)} meta={8} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: idCont + 3,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[8] <= 50) {
              if (numero == 1) {
                return 'Eso esta mejor'
              } else {
                if (numero == 2) {
                  return 'Sigue así'
                } else {
                  return 'Sigue así'
                }
              }
            } else {
              if (numero == 1) {
                return 'Genial'
              } else {
                if (numero == 2) {
                  return 'Que bien'
                } else {
                  return 'Que bien'
                }
              }
            }
          },
          trigger: idCont + 4,
        },
        {
          id: idCont + 4,
          message: 'Los lácteos son muy necesarios, pero tambien poseen alta cantidad de grasa',
          trigger: idCont + 5,
        },
        {
          id: idCont + 5,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Es preferible consumir los lácteos bajos en grasa'
            } else {
              if (numero == 2) {
                return 'Son deliciosos y acompañan muy bien las comidas'
              } else {
                return 'Son deliciosos y acompañan muy bien las comidas'
              }
            }
          },
          trigger: idCont + 6
        },
        {
          id: idCont + 6,
          message: 'Sigue comiendo este tipo de alimentos ',
          trigger: idCont + 11,
        },
        {
          id: idCont + 7,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[8] <= 50) {
              return 'No sigas consumiendo los lácteos grasos'
            } else {
              if (numero == 1) {
                return 'Intentalo'
              } else {
                if (numero == 2) {
                  return 'Son muy ricos'
                } else {
                  return 'Son muy ricos'
                }
              }
            }
          },
          trigger: idCont + 8,
        },
        {
          id: idCont + 8,
          message: 'Los productos grasos le hacen mucho daño a nuestro corazón y a nuestro rendimiento diario.',
          trigger: idCont + 9,
        },
        {
          id: idCont + 9,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Es preferible consumir los lácteos bajos en grasa'
            } else {
              if (numero == 2) {
                return 'Son deliciosos y acompañan muy bien las comidas'
              } else {
                return 'Son deliciosos y acompañan muy bien las comidas'
              }
            }
          },
          trigger: idCont + 10,
        },
        {
          id: idCont + 10,
          message: () => {
            if (score[8] <= 50) {
              return 'Tienes que intentar cumplir tus objetivos'
            } else {
              return 'No falles la proxíma'
            }
          },
          trigger: idCont + 11,
        },
      );

      idCont = idCont + 11;

    }
    if (metas.includes(9) && this.validateDateOfQuestion(9, reuse)) {
      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Dime, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return 'Cuéntame, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return '¡' + this.state.nombre + ' !'
                } else {
                  return '¡' + this.state.nombre + ' !'
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿ Cuantas porciones de galletas, dulces, pastas o pasteles has comido hoy ?',
          trigger: idCont + 2,
        },
        {
          id: idCont + 2,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={1} currentStep={(idCont + 2)} meta={9} menorQueCorrecto={true} />),
          waitAction: true
        },
        {
          id: idCont + 3,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[9] <= 50) {
              if (numero == 1) {
                return 'Eso esta mejor'
              } else {
                if (numero == 2) {
                  return 'Sigue así'
                } else {
                  return 'Sigue así'
                }
              }
            } else {
              if (numero == 1) {
                return 'Genial'
              } else {
                if (numero == 2) {
                  return 'Que bien'
                } else {
                  return 'Que bien'
                }
              }
            }
          },
          trigger: idCont + 4,
        },
        {
          id: idCont + 4,
          message: 'Este tipo de productos son los mas peligrosos para el azúcar en la sangre',
          trigger: idCont + 5,
        },
        {
          id: idCont + 5,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Es preferible eliminarlos de la dieta'
            } else {
              if (numero == 2) {
                return 'Come otro tipo de snakcs, como frutas o frutos secos'
              } else {
                return 'Come otro tipo de snakcs, como frutas o frutos secos'
              }
            }
          },
          trigger: idCont + 6
        },
        {
          id: idCont + 6,
          message: 'Sigue así ',
          trigger: idCont + 11,
        },
        {
          id: idCont + 7,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[9] <= 50) {
              return 'Esos productos son los que mas te hacen daño'
            } else {
              if (numero == 1) {
                return 'No esta bien'
              } else {
                if (numero == 2) {
                  return 'Se te puede subir el azúcar'
                } else {
                  return 'Se te puede subir el azúcar'
                }
              }
            }
          },
          trigger: idCont + 8,
        },
        {
          id: idCont + 8,
          message: 'Si sigues asi tu diabetes puede evolucionar',
          trigger: idCont + 9,
        },
        {
          id: idCont + 9,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Es preferible eliminarlos de la dieta'
            } else {
              if (numero == 2) {
                return 'Come otro tipo de snakcs, como frutas o frutos secos'
              } else {
                return 'Come otro tipo de snakcs, como frutas o frutos secos'
              }
            }
          },
          trigger: idCont + 10,
        },
        {
          id: idCont + 10,
          message: () => {
            if (score[9] <= 50) {
              return 'Tienes que intentar cumplir tus objetivos'
            } else {
              return 'No falles la proxíma'
            }
          },
          trigger: idCont + 11,
        },
      );

      idCont = idCont + 11;

    }

    if (metas.includes(10) && this.validateDateOfQuestion(10, reuse)) {
      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Dime, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return 'Cuéntame, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return '¡' + this.state.nombre + ' !'
                } else {
                  return '¡' + this.state.nombre + ' !'
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿ Cuantas porciones de comida rápida has comido hoy ?',
          trigger: idCont + 2,
        },
        {
          id: idCont + 2,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={0} currentStep={(idCont + 2)} meta={10} menorQueCorrecto={true} />),
          waitAction: true
        },
        {
          id: idCont + 3,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[10] <= 50) {
              if (numero == 1) {
                return 'Eso esta mejor'
              } else {
                if (numero == 2) {
                  return 'Sigue así'
                } else {
                  return 'Sigue así'
                }
              }
            } else {
              if (numero == 1) {
                return 'Genial'
              } else {
                if (numero == 2) {
                  return 'Que bien'
                } else {
                  return 'Que bien'
                }
              }
            }
          },
          trigger: idCont + 4,
        },
        {
          id: idCont + 4,
          message: 'Hay que eliminar ese tipo de comida de nuestra dieta',
          trigger: idCont + 5,
        },
        {
          id: idCont + 5,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Solo le hacen daño a nuestro cuerpo'
            } else {
              if (numero == 2) {
                return 'Contienen mucha cantidad de grasas'
              } else {
                return 'Contienen mucha cantidad de grasas'
              }
            }
          },
          trigger: idCont + 6
        },
        {
          id: idCont + 6,
          message: 'Mantente sin comerlos ',
          trigger: idCont + 11,
        },
        {
          id: idCont + 7,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[10] <= 50) {
              return 'No sigas consumiendo estas comidas, te haces daño'
            } else {
              if (numero == 1) {
                return 'No lo hagas otra vez'
              } else {
                if (numero == 2) {
                  return 'Eliminalos'
                } else {
                  return 'Eliminalos'
                }
              }
            }
          },
          trigger: idCont + 8,
        },
        {
          id: idCont + 8,
          message: 'La cantidad de grasas que contienen estos productos es muy elevada',
          trigger: idCont + 9,
        },
        {
          id: idCont + 9,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Solo le hacen daño a nuestro cuerpo'
            } else {
              if (numero == 2) {
                return 'Contienen mucha cantidad de grasas'
              } else {
                return 'Contienen mucha cantidad de grasas'
              }
            }
          },
          trigger: idCont + 10,
        },
        {
          id: idCont + 10,
          message: () => {
            if (score[10] <= 50) {
              return 'Tienes que intentar cumplir tus objetivos'
            } else {
              return 'No falles la proxíma'
            }
          },
          trigger: idCont + 11,
        },
      );

      idCont = idCont + 11;

    }

    if (metas.includes(11) && this.validateDateOfQuestion(11, reuse)) {
      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Dime, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return 'Cuéntame, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return '¡' + this.state.nombre + ' !'
                } else {
                  return '¡' + this.state.nombre + ' !'
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿ Cuantas vasos de bebidas azucaradas has tomado hoy ?',
          trigger: idCont + 2,
        },
        {
          id: idCont + 2,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={0} currentStep={(idCont + 2)} meta={11} menorQueCorrecto={true} />),
          waitAction: true
        },
        {
          id: idCont + 3,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[11] <= 50) {
              if (numero == 1) {
                return 'Eso esta mejor'
              } else {
                if (numero == 2) {
                  return 'Sigue así'
                } else {
                  return 'Sigue así'
                }
              }
            } else {
              if (numero == 1) {
                return 'Genial'
              } else {
                if (numero == 2) {
                  return 'Que bien'
                } else {
                  return 'Que bien'
                }
              }
            }
          },
          trigger: idCont + 4,
        },
        {
          id: idCont + 4,
          message: 'Hay que eliminar ese tipo de bebidas de nuestra dieta',
          trigger: idCont + 5,
        },
        {
          id: idCont + 5,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Solo le hacen daño a nuestro cuerpo'
            } else {
              if (numero == 2) {
                return 'Contienen mucha cantidad de grasas'
              } else {
                return 'Contienen mucha cantidad de grasas'
              }
            }
          },
          trigger: idCont + 6
        },
        {
          id: idCont + 6,
          message: 'Mantente sin tomarlas ',
          trigger: idCont + 11,
        },
        {
          id: idCont + 7,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[11] <= 50) {
              return 'No sigas consumiendo estas bebidas, te haces daño'
            } else {
              if (numero == 1) {
                return 'No lo hagas otra vez'
              } else {
                if (numero == 2) {
                  return 'Eliminalas'
                } else {
                  return 'Eliminalas'
                }
              }
            }
          },
          trigger: idCont + 8,
        },
        {
          id: idCont + 8,
          message: 'La cantidad de azucar que contienen estos productos es muy elevada',
          trigger: idCont + 9,
        },
        {
          id: idCont + 9,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Solo le hacen daño a nuestro cuerpo'
            } else {
              if (numero == 2) {
                return 'Contienen mucha cantidad de azucares y quimicos'
              } else {
                return 'Contienen mucha cantidad de azucares y quimicos'
              }
            }
          },
          trigger: idCont + 10,
        },
        {
          id: idCont + 10,
          message: () => {
            if (score[11] <= 50) {
              return 'Tienes que intentar cumplir tus objetivos'
            } else {
              return 'No falles la proxíma'
            }
          },
          trigger: idCont + 11,
        },
      );

      idCont = idCont + 11;

    }

    if (metas.includes(12) && this.validateDateOfQuestion(12, reuse)) {
      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Dime, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return 'Cuéntame, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return '¡' + this.state.nombre + ' !'
                } else {
                  return '¡' + this.state.nombre + ' !'
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿ Cuantas porciones de carnes rojas o productos cárnicos has comido  hoy ?',
          trigger: idCont + 2,
        },
        {
          id: idCont + 2,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={2} currentStep={(idCont + 2)} meta={12} menorQueCorrecto={true} />),
          waitAction: true
        },
        {
          id: idCont + 3,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[12] <= 50) {
              if (numero == 1) {
                return 'Eso esta mejor'
              } else {
                if (numero == 2) {
                  return 'Sigue así'
                } else {
                  return 'Sigue así'
                }
              }
            } else {
              if (numero == 1) {
                return 'Genial'
              } else {
                if (numero == 2) {
                  return 'Que bien'
                } else {
                  return 'Que bien'
                }
              }
            }
          },
          trigger: idCont + 4,
        },
        {
          id: idCont + 4,
          message: 'Las carnes rojas estan asociadas a diversos problemas',
          trigger: idCont + 5,
        },
        {
          id: idCont + 5,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Suelen estar ligadas con el cáncer y otras enfermedades'
            } else {
              if (numero == 2) {
                return 'Tienen mas porcentaje de grasas con otros productos'
              } else {
                return 'Tienen mas porcentaje de grasas con otros productos'
              }
            }
          },
          trigger: idCont + 6
        },
        {
          id: idCont + 6,
          message: 'Mantente sin comerlos ',
          trigger: idCont + 11,
        },
        {
          id: idCont + 7,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (score[12] <= 50) {
              return 'No sigas consumiendo este tipo de carne, te haces daño'
            } else {
              if (numero == 1) {
                return 'No lo hagas otra vez'
              } else {
                if (numero == 2) {
                  return 'Eliminalas'
                } else {
                  return 'Eliminalas'
                }
              }
            }
          },
          trigger: idCont + 8,
        },
        {
          id: idCont + 8,
          message: 'La cantidad de problemas asociados es muy grande',
          trigger: idCont + 9,
        },
        {
          id: idCont + 9,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Suelen estar ligadas con el cáncer y otras enfermedades'
            } else {
              if (numero == 2) {
                return 'Tienen mas porcentaje de grasas con otros productos'
              } else {
                return 'Tienen mas porcentaje de grasas con otros productos'
              }
            }
          },
          trigger: idCont + 10,
        },
        {
          id: idCont + 10,
          message: () => {
            if (score[12] <= 50) {
              return 'Tienes que intentar cumplir tus objetivos'
            } else {
              return 'No falles la proxíma'
            }
          },
          trigger: idCont + 11,
        },
      );

      idCont = idCont + 11;

    }

    if (this.validateDateOfQuestion(-1, reuse)) {
      steps.push(
        {
          id: idCont,
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 4) + 4)
            if (numero == 1) {
              return 'Dime, ' + this.state.nombre
            } else {
              if (numero == 2) {
                return 'Cuéntame, ' + this.state.nombre + '?'
              } else {
                if (numero == 3) {
                  return '¡' + this.state.nombre + ' !'
                } else {
                  return '¡' + this.state.nombre + ' !'
                }
              }
            }
          },
          trigger: idCont + 1,
        },
        {
          id: idCont + 1,
          message: '¿Te has vuelto a pesar?',
          trigger: idCont + 2,
        },
        {
          id:idCont+2,
          options: [
            { value: 1, label: 'Sí', trigger: (idCont + 3) },
            { value: 2, label: 'No', trigger: (idCont + 6) },
          ]
        },
        {
          id: idCont + 3,
          component: (<RespuestaChatBot ifIsReuse={false} correctValue={2} currentStep={(idCont + 3)} meta={-1} menorQueCorrecto={true} />),
          waitAction: true
        },
        {
          id: idCont + 4,
          message: 'Vale, la proxima semana te estare preguntando nuevamente',
          trigger: idCont + 7
        },
        {
          id:idCont+6,
          message:'Vale, pero pesate cuanto antes y me comentas el resultado',
          trigger:idCont+7
        }
      );
      idCont = idCont + 7;
    }

    if (ejerciciosHoy === "Si") {
      const { currentUser } = firebase.auth()
      firebase.database().ref('progreso/usuarios/' + currentUser.uid + '/metas/1/').push({
        'fecha': '2018-09-24 20:00',
        'score': '1'
      }).then((data) => {
        //success callback
      }).catch((error) => {
        //error callback
      })
    }


    if (steps.length >= 1) {
      steps.push(
        {
          id: idCont,
          message: 'Bueno, espero estes muy bien! Hasta mañana.',
          end: true
        }
      )
      return <ChatBot steps={steps} />
    }
    else {
      steps.push(
        {
          id: idCont,
          message: 'Hola! Parece que ya me contastes de tu progreso. Genial!',
          trigger: idCont + 1
        }, {
          id: idCont + 1,
          message: 'Deseas hablarme de alguna de tus metas?',
          trigger: idCont + 2
        },
        {
          id: idCont + 2,
          options: [
            { value: 1, label: 'Sí', trigger: (idCont + 3) },
            { value: 2, label: 'No', trigger: (idCont + 4) },
          ]
        },
        {
          id: idCont + 4,
          message: 'Bueno, espero estes muy bien! Hasta mañana.',
          end: true
        },
        {
          id: idCont + 3,
          component: <ReUse change={this.changeReuse} display={this.displayChatBot.bind(this)} currentStep={idCont + 3} metas={metas} />,
          waitAction: true
        },
        {
          id: 'reuse1-1',
          message: 'Cuantos minutos de ejercicios has hecho hoy?',
          trigger: 'reuse1-2'
        },
        {
          id: 'reuse1-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={22} currentStep={3} meta={1} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: 'reuse2-1',
          message: '¿ Hoy cuantos ejercicios de intensidad moderada realizaste ?',
          trigger: 'reuse2-2'
        },
        {
          id: 'reuse2-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={1} currentStep={3} meta={2} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: 'reuse4-1',
          message: '¿ Cuantas porciones de frutas has comido ?',
          trigger: 'reuse4-2'
        },
        {
          id: 'reuse4-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={3} currentStep={3} meta={4} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: 'reuse3-1',
          message: '¿ Cuantas porciones de verduras has comido ?',
          trigger: 'reuse3-2'
        },
        {
          id: 'reuse3-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={5} currentStep={3} meta={3} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: 'reuse5-1',
          message: '¿ Cuantas cucharadas de aceite de oliva extra virgen has tomado ?',
          trigger: 'reuse5-2'
        },
        {
          id: 'reuse5-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={4} currentStep={3} meta={5} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: 'reuse6-1',
          message: '¿ Cuantas cucharadas de aceite de oliva extra virgen has tomado ?',
          trigger: 'reuse6-2'
        },
        {
          id: 'reuse6-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={3} currentStep={3} meta={6} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: 'reuse7-1',
          message: '¿ Cuantos alimentos integrales has comido hoy ?',
          trigger: 'reuse7-2'
        },
        {
          id: 'reuse7-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={4} currentStep={3} meta={7} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: 'reuse8-1',
          message: '¿ Cuantos productos lácteos bajos en grasa has comido hoy ?',
          trigger: 'reuse8-2'
        },
        {
          id: 'reuse8-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={1} currentStep={3} meta={8} menorQueCorrecto={false} />),
          waitAction: true
        },
        {
          id: 'reuse9-1',
          message: '¿ Cuantas porciones de galletas, dulces, pastas o pasteles has comido hoy ?',
          trigger: 'reuse9-2'
        },
        {
          id: 'reuse9-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={1} currentStep={3} meta={9} menorQueCorrecto={true} />),
          waitAction: true
        },
        {
          id: 'reuse10-1',
          message: '¿ Cuantas porciones de comida rápida has comido hoy ?',
          trigger: 'reuse10-2'
        },
        {
          id: 'reuse10-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={0} currentStep={3} meta={10} menorQueCorrecto={true} />),
          waitAction: true
        },
        {
          id: 'reuse11-1',
          message: '¿ Cuantas vasos de bebidas azucaradas has tomado hoy ?',
          trigger: 'reuse11-2'
        },
        {
          id: 'reuse11-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={0} currentStep={3} meta={11} menorQueCorrecto={true} />),
          waitAction: true
        },
        {
          id: 'reuse12-1',
          message: '¿ Cuantas porciones de carnes rojas o productos cárnicos has comido  hoy ?',
          trigger: 'reuse12-2'
        },
        {
          id: 'reuse12-2',
          component: (<RespuestaChatBot ifIsReuse={true} correctValue={2} currentStep={3} meta={12} menorQueCorrecto={true} />),
          waitAction: true
        },

        {
          id: 'end-reuse',
          message: () => {
            var numero = Math.floor(Math.random() * (1 - 3) + 3)
            if (numero == 1) {
              return 'Listo, se han registrado tus datos'
            } else {
              return 'Perfecto, lo he registrado'
            }
          },
          end: true
        }
      )

      return <ChatBot steps={steps} />
    }


  }



}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
