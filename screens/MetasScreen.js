import React from 'react';
import { StyleSheet, View, StatusBar, ImageBackground, ScrollView } from 'react-native';
import { StackNavigator, createBottomTabNavigator } from 'react-navigation';
import { Header, Left, Right, Icon, Text, Card, CardItem, Body } from 'native-base';
import firebase from 'firebase';
import { Button } from 'react-native-elements';
import moment from 'moment';
import Emoji from 'react-native-emoji';

const user = {};
const METAS = [];
const scores = [];
const progreso = [];



class MetasSemanaAnterior extends React.Component {
    state = { currentUser: null, email: null, estatura: null, metas: [], score: 0 }
    componentDidMount() {
        const { currentUser } = this.state;
    }

    componentWillMount() {
        const { currentUser } = firebase.auth()
        this.setState({ currentUser })
        var year = moment().format('YYYY');
        var week = moment().format('WW');
        firebase.database().ref('/usuarios/' + currentUser.uid).once('value', (snapshot) => {
            user = snapshot.val();
            metas = user.metas;
            METAS = [];
            this.setState({ email: user.email, estatura: user.estatura });
            that = this;
            metasArray = [];
            
            if (metas.constructor === Array) {
                for (let index = 0; index < metas.length; index++) {
                    const element = metas[index];
                    if(typeof metas[index]!=='undefined'){
                        metasArray.push({'meta':element,key:index})
                    }
                }
            } else {
                console.log("3333")
                Object.keys(metas).map((meta) => {
                    console.log("element2",meta[meta])
                    metasArray.push({ 'meta': metas[meta], key: meta });
                })
            }
            console.log("metas",metasArray);
            metasArray.forEach(function (value) {
                var key = value.key
                firebase.database().ref('/progreso/usuarios/' + currentUser.uid + '/' + year + '/' + (week - 1) + '/metas/' + key + '/')
                    .once('value', (snapshot) => {
                        progreso = snapshot.val();
                        if (progreso !== null) {
                            var progresoArray = [];
                            progreso = Object.keys(progreso).map(function (key) {
                                progresoArray.push(
                                    progreso[key]
                                );
                            });
                            progreso = progresoArray;
                            var respuestas = 0;
                            progreso.forEach(function (element) {
                                respuestas += Number(element["respuesta"]);
                            });
                            // var keyObj = "";
                            // keyObj = Object.keys(snapshot.val())[Object.keys(snapshot.val()).length - 1];
                            // var progresoObject = progreso[keyObj + ""];
                            // var progresoScore = progresoObject["score"];
                            METAS.push({
                                descripcion: value.meta,
                                metaid: key,
                                score: respuestas
                            });
                            // this.setState({ metas: METAS });
                        } else {
                            METAS.push({
                                descripcion: value.meta,
                                metaid: key,
                                score: 'Aun no has registrado datos.'
                            });
                        }

                        that.setState({ metas: METAS })
                    }).then(

                    );
            });

        });


    }

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <Icon name="flag" style={{ fontSize: 24, color: tintColor }} />
        )
    }

    calcularScoreTotal = (array) => {
        if (array.length >= 3 && this.state.score === 0) {
            var score = 0;
            var cont = 0;
            array.forEach(element => {
                score += element["score"];
                cont++;
            });
            score = score / cont;
            this.setState({ score: score });
        }

    }

    calcularComentario = (score, meta) => {

        if (typeof (score) !== 'string') {
            switch (meta) {
                case 1:
                    if (Number(score) < 150) {
                        score = score + ' Minutos';
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;

                case 2:
                    if (Number(score) < 2) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 3:
                    if (Number(score) < 35) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 4:
                    if (Number(score) < 21) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 5:
                    if (Number(score) < 28) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 6:
                    if (Number(score) < 3) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 7:
                    if (Number(score) < 28) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 8:
                    if (Number(score) < 7) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 9:
                    if (Number(score) > 1) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 10:
                    if (Number(score) > 1) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 11:
                    if (Number(score) >= 1) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 12:
                    if (Number(score) > 2) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                default:
                    return (
                        <Text style={{ textAlign: 'center' }}>
                            <Emoji name="muscle" style={{ fontSize: 30 }} />
                            {"  "}Animo, tu puedes dar mas!{"  "}
                            <Emoji name="muscle" style={{ fontSize: 30 }} />
                        </Text>);
                    break;
            }
        } else {
            return null;
        }

    }

    getUnidad(key, score) {
        if (typeof (score) !== 'string') {
            switch (key) {
                case 1:
                    if (score === 1) return "minuto"
                    else return "minutos"
                    break;
                case 2:
                    if (score === 1) return "ejercicio"
                    else return "ejercicios"
                    break;
                case 3:
                case 9:
                case 12:
                case 7:
                    if (score === 1) return "porción"
                    else return "porciones"
                case 4:
                case 6:
                case 10:
                    if (score === 1) return "ración"
                    else return "raciones"
                case 8:
                    if (score === 1) return "producto lácteo"
                    else return "productos lácteos"
                case 5:
                    if (score === 1) return "cucharada"
                    else return "cucharadas"
                case 11:
                    if (score === 1) return "vaso"
                    else return "vasos"
                default:
                    return "";
            }
        }
        else return "";
    }

    render() {

        return (
            <ScrollView>
                <Header>
                    <Left style={{ flex: 1 }}>
                        <Icon name="menu" onPress={() => this.props.navigation.openDrawer()} />
                    </Left>
                </Header>

                <Card>
                    <CardItem bordered>
                        <Body>
                            <Text style={{ fontSize: 24, margin: 5, marginTop: 10 }}>Tus Metas - Semana Anterior</Text>
                        </Body>
                    </CardItem>

                    {this.calcularScoreTotal(this.state.metas)}
                    {
                        (this.state.metas).map((element, i) => {
                            return (
                                <CardItem bordered key={i}>
                                    <Body>
                                        <Text style={{ fontSize: 20, marginLeft: 8 }}>{element.descripcion}.</Text>
                                        <Text style={{ fontSize: 15 }}>Tu resultado de la semana anterior:</Text>
                                        <Text style={{ fontSize: 15, fontWeight: "bold" }}>{" "}{(element.score)}{" "}{this.getUnidad(Number(element.metaid), element.score)}</Text>
                                        <Text style={{ fontSize: 17 }}>{"\n"}{this.calcularComentario(element.score, Number(element.metaid))}</Text>

                                        {/* <View style={{ alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                                        <Button
                                            raised
                                            icon={{ name: 'send' }}
                                            title=""
                                            backgroundColor="#00f"
                                            style={{ width: 100 }}
                                        />
                                    </View> */}
                                    </Body>
                                </CardItem>
                            )
                        }
                        )
                    }
                </Card>
            </ScrollView>
        )
    }
}

class MetasScreen extends React.Component {
    state = { currentUser: null, email: null, estatura: null, metas: [], score: 0 }
    componentDidMount() {
        const { currentUser } = this.state;
    }

    componentWillMount() {
        const { currentUser } = firebase.auth()
        this.setState({ currentUser })
        var year = moment().format('YYYY');
        var week = moment().format('WW');
        firebase.database().ref('/usuarios/' + currentUser.uid).once('value', (snapshot) => {
            user = snapshot.val();
            metas = user.metas;
            METAS = [];
            this.setState({ email: user.email, estatura: user.estatura });
            that = this;
            metasArray = [];
            if (metas.constructor === Array) {
                for (let index = 0; index < metas.length; index++) {
                    const element = metas[index];
                    if(typeof metas[index]!=='undefined'){
                        metasArray.push({'meta':element,key:index})
                    }
                }
            } else {
                Object.keys(metas).map((meta) => {
                    metasArray.push({ 'meta': metas[meta], key: meta });
                })
            }

            metasArray.forEach(function (value) {
                var key = value.key
                firebase.database().ref('/progreso/usuarios/' + currentUser.uid + '/' + year + '/' + week + '/metas/' + key + '/')
                    .once('value', (snapshot) => {
                        progreso = snapshot.val();
                        if (progreso !== null) {
                            var progresoArray = [];
                            progreso = Object.keys(progreso).map(function (key) {
                                progresoArray.push(
                                    progreso[key]
                                );
                            });
                            progreso = progresoArray;
                            var respuestas = 0;
                            progreso.forEach(function (element) {
                                respuestas += Number(element["respuesta"]);
                            });
                            // var keyObj = "";
                            // keyObj = Object.keys(snapshot.val())[Object.keys(snapshot.val()).length - 1];
                            // var progresoObject = progreso[keyObj + ""];
                            // var progresoScore = progresoObject["score"];
                            METAS.push({
                                descripcion: value.meta,
                                metaid: key,
                                score: respuestas
                            });
                            // this.setState({metas: METAS });
                        } else {
                            METAS.push({
                                descripcion: value.meta,
                                metaid: key,
                                score: 'Aun no has registrado datos.'
                            });
                        }

                        that.setState({ metas: METAS })
                    }).then(

                    );
            });

        });


    }

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <Icon name="flag" style={{ fontSize: 24, color: tintColor }} />
        )
    }

    calcularScoreTotal = (array) => {
        if (array.length >= 3 && this.state.score === 0) {
            var score = 0;
            var cont = 0;
            array.forEach(element => {
                score += element["score"];
                cont++;
            });
            score = score / cont;
            this.setState({ score: score });
        }

    }

    calcularComentario = (score, meta) => {

        if (typeof (score) !== 'string') {
            switch (meta) {
                case 1:
                    if (Number(score) < 150) {
                        score = score + ' Minutos';
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;

                case 2:
                    if (Number(score) < 2) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 3:
                    if (Number(score) < 35) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 4:
                    if (Number(score) < 21) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 5:
                    if (Number(score) < 28) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 6:
                    if (Number(score) < 3) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 7:
                    if (Number(score) < 28) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 8:
                    if (Number(score) < 7) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 9:
                    if (Number(score) > 1) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 10:
                    if (Number(score) > 1) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 11:
                    if (Number(score) >= 1) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                case 12:
                    if (Number(score) > 2) {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                                {"  "}Animo, tu puedes dar mas!{"  "}
                                <Emoji name="muscle" style={{ fontSize: 30 }} />
                            </Text>);
                    } else {
                        return (
                            <Text style={{ textAlign: 'center' }}>
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                                {"  "}Sigue asi!{"  "}
                                <Emoji name="tada" style={{ fontSize: 30 }} />
                            </Text>);
                    }
                    break;
                default:
                    return (
                        <Text style={{ textAlign: 'center' }}>
                            <Emoji name="muscle" style={{ fontSize: 30 }} />
                            {"  "}Animo, tu puedes dar mas!{"  "}
                            <Emoji name="muscle" style={{ fontSize: 30 }} />
                        </Text>);
                    break;
            }
        } else {
            return null;
        }

    }

    getUnidad(key, score) {
        if (typeof (score) !== 'string') {
            switch (key) {
                case 1:
                    if (score === 1) return "minuto"
                    else return "minutos"
                    break;
                case 2:
                    if (score === 1) return "ejercicio"
                    else return "ejercicios"
                    break;
                case 3:
                case 9:
                case 12:
                case 7:
                    if (score === 1) return "porción"
                    else return "porciones"
                case 4:
                case 6:
                case 10:
                    if (score === 1) return "ración"
                    else return "raciones"
                case 8:
                    if (score === 1) return "producto lácteo"
                    else return "productos lácteos"
                case 5:
                    if (score === 1) return "cucharada"
                    else return "cucharadas"
                case 11:
                    if (score === 1) return "vaso"
                    else return "vasos"
                default:
                    return "";
            }
        }
        else return "";
    }
    render() {

        return (
            <ScrollView style={{ height: '90%' }}>
                <Header>
                    <Left style={{ flex: 1 }}>
                        <Icon name="menu" onPress={() => this.props.navigation.openDrawer()} />
                    </Left>
                </Header>

                <Card>
                    <CardItem bordered>
                        <Body>
                            <Text style={{ fontSize: 24, margin: 5, marginTop: 10 }}>Tus Metas - Semana Actual</Text>
                        </Body>
                    </CardItem>
                    {this.calcularScoreTotal(this.state.metas)}
                    {
                        (this.state.metas).map((element, i) => {

                            return (
                                <CardItem bordered key={i}>
                                    <Body>
                                        <Text style={{ fontSize: 20, marginLeft: 8 }}>{element.descripcion}.</Text>
                                        <Text style={{ fontSize: 15 }}>Tu resultado de la semana actual:</Text>
                                        <Text style={{ fontSize: 15, fontWeight: "bold" }}>{" "}{(element.score)}{" "}{this.getUnidad(Number(element.metaid), element.score)}</Text>
                                        <Text style={{ fontSize: 17 }}>{"\n"}{this.calcularComentario(element.score, Number(element.metaid))}</Text>

                                        {/* <View style={{ alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                                    <Button
                                        raised
                                        icon={{ name: 'send' }}
                                        title=""
                                        backgroundColor="#00f"
                                        style={{ width: 100 }}
                                    />
                                </View> */}
                                    </Body>
                                </CardItem>
                            )
                        }
                        )
                    }
                </Card>
                {/* <Card>
                        <CardItem>
                            <Text style={{ fontSize: 18, fontFamily: "Arial", fontWeight: "bold" }}>Score total: {(this.state.score).toFixed(2)}</Text>
                        </CardItem>
                    </Card> */}


            </ScrollView >
        )
    }
}

export default createBottomTabNavigator({
    'Esta Semana': MetasScreen,
    'Semana Anterior': MetasSemanaAnterior,
}, {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Esta Semana') {
                    iconName = `ios-arrow-dropdown${focused ? '-circle-outline' : ''}`;
                } else if (routeName === 'Semana Anterior') {
                    iconName = `ios-archive${focused ? '' : '-outline'}`;
                }

                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return <Icon name={iconName} size={horizontal ? 20 : 25} color={tintColor} />;
            }

        }),
        tabBarOptions: {
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
        },
    });


const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
