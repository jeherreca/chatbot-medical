import React from 'react';
import {Text, View, AppRegistry} from 'react-native';

const Header=(props)=>{

	const {viewStyle, textStyle}= Styles;
	return (
	<View style={viewStyle}>
		<Text style={textStyle}>{props.headerText}</Text>
	</View>
	);
}

const Styles={
	textStyle:{
		fontSize: 30,
    	color: '#000000'
	},
	viewStyle:{
		justifyContent: 'center',
		alignItems: 'center',
		height:80,
		paddingTop: 30,
		elevation: 0,
		position: 'relative'
	}

};


export default Header;